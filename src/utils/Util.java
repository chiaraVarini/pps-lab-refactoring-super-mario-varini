package utils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by chiaravarini on 05/03/17.
 */
public final class Util {

    private Util(){}

    public static ImageIcon getImageIcon(final String imageName){
        return new ImageIcon(Util.class.getResource("/images/"+imageName));
    }

    public static AudioInputStream getAudio(final String audioName) {
        try {
            return  AudioSystem.getAudioInputStream(Util.class.getResource(audioName));

        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
