package characters;

import java.awt.Image;

import game.Main;
import gameObject.GameObject;
import utils.Util;

public class CharacterImpl {

    private int width, height;
    private int intialPositionX, intialPositionY;
    private boolean isWalking;
    private boolean turnToRight;
    private int stepCounter;
    private boolean isAlive;


    public CharacterImpl(int intialPositionX, int intialPositionY, int width, int height){
        this.intialPositionX = intialPositionX;
        this.intialPositionY = intialPositionY;
        this.height = height;
        this.width = width;
        this.stepCounter = 0;
        this.isWalking = false ;
        this.turnToRight = true ;
        this.isAlive = true;
    }


    // getters

    public boolean isAlive() {
        return isAlive;
    }

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public int getIntialPositionX() {
        return intialPositionX;
    }
    public int getIntialPositionY() {
        return intialPositionY;
    }
    public boolean isWalking() {
        return isWalking;
    }
    public boolean isTurnToRight() {
        return turnToRight;
    }
    public int getStepCounter() {
        return stepCounter;
    }

    // setters

    public void setAlive(boolean alive) {
        this.isAlive = alive;
    }
    public void setIntialPositionX(int intialPositionX) {
        this.intialPositionX = intialPositionX;
    }
    public void setIntialPositionY(int intialPositionY) {
        this.intialPositionY = intialPositionY;
    }
    public void setWalking(boolean walking) {
        this.isWalking = walking;
    }
    public void setTurnToRight(boolean turnToRight) {
        this.turnToRight = turnToRight;
    }
    public void setStepCounter(int stepCounter) {
        this.stepCounter = stepCounter;
    }
    public void setIsAlive(boolean isAlive){this.isAlive = isAlive; }

    //metodo per gestire i movimenti dei personagi
    public Image walk(String nome , int frequenza ){

        String str ;

        if(!this.isWalking()){

            if(this.isTurnToRight()){ // se guarda a destra
                str =  nome + "AD.png";
            }else str =  nome + "AG.png"; //se non
        }else {
            this.stepCounter++;
            if(this.stepCounter / frequenza == 0){
                if(this.isTurnToRight()){
                    str =   nome + "AD.png"; // mario fermo a destra
                }else str =  nome + "AG.png";// mario fermo a sinistra
            }else{
                if(this.isTurnToRight()){
                    str = nome + "D.png"; // mario che camina verso destra
                }else str =   nome + "G.png";// mario che camina verso sinistra
            }
            if(this.stepCounter == 2* frequenza )
                this.stepCounter = 0 ;
        }

        //images del personnagio
        return Util.getImageIcon(str).getImage();
    }



    public void spostamenti (){
        if(Main.platform.getxPos() >= 0){
            this.intialPositionX = this.intialPositionX - Main.platform.getMov();
        }
    }

    // detezione objectCotact a destra
    public boolean contactAvanti(GameObject og){
        return !(this.intialPositionX + this.width < og.getX() ||
                this.intialPositionX + this.width > og.getX() + 5 ||
                this.intialPositionY + this.height <= og.getY() ||
                this.intialPositionY >= og.getY() + og.getHeight());
    }

    // detezione objectCotact a sinistra
    protected boolean contactIndietro(GameObject og){
        return !(this.intialPositionX > og.getX() + og.getWidth() ||
                this.intialPositionX + this.width < og.getX() + og.getWidth() -5 ||
                this.intialPositionY + this.height <= og.getY() ||
                this.intialPositionY >= og.getY() + og.getHeight());
    }

    //objectCotact sotto
    protected boolean contactSotto(GameObject og){
        return !(this.intialPositionX + this.width < og.getX() + 5 ||
                this.intialPositionX > og.getX() + og.getWidth() - 5 ||
                this.intialPositionY + this.height < og.getY() ||
                this.intialPositionY +this.height > og.getY() + 5 );
    }

    // objectCotact in alto
    protected boolean contactAlto(GameObject og){
        return !(this.intialPositionX + this.width < og.getX() + 5 ||
                this.intialPositionX > og.getX() + og.getWidth() - 5 ||
                this.intialPositionY < og.getY() + og.getHeight() ||
                this.intialPositionY > og.getY()+ og.getHeight() +5 );
    }

    public boolean vicino(GameObject obj){

        return ((this.intialPositionX > obj.getX() - 10 &&
                this.intialPositionX < obj.getX() + obj.getWidth() + 10) ||
                (this.getIntialPositionX()+ this.width > obj.getX() - 10 &&
                        this.intialPositionX + this.width < obj.getX() + obj.getWidth() + 10));
    }

    //objectCotact fra personnaggi
    protected boolean contactAvanti(CharacterImpl pers){
        if(this.isTurnToRight()){
            return !(this.intialPositionX + this.width < pers.getIntialPositionX() ||
                    this.intialPositionX + this.width > pers.getIntialPositionX() + 5 ||
                    this.intialPositionY + this.height <= pers.getIntialPositionY() ||
                    this.intialPositionY >= pers.getIntialPositionY() + pers.getHeight());
        }else{return false;}
    }

    protected boolean contactIndietro(CharacterImpl pers){
        return !(this.intialPositionX > pers.getIntialPositionX() + pers.getWidth() ||
                this.intialPositionX + this.width < pers.getIntialPositionX() + pers.getWidth() - 5 ||
                this.intialPositionY + this.height <= pers.getIntialPositionY() ||
                this.intialPositionY >= pers.getIntialPositionY() +pers.getHeight());
    }

    public boolean contactSotto(CharacterImpl pers){
        return !(this.intialPositionX + this.width < pers.getIntialPositionX() ||
                this.intialPositionX > pers.getIntialPositionX() + pers.getWidth() ||
                this.intialPositionY + this.height < pers.getIntialPositionY() ||
                this.intialPositionY + this.height > pers.getIntialPositionY());

    }
    //vicino fra personnagi
    public boolean vicino(CharacterImpl pers){
         return ((this.intialPositionX > pers.getIntialPositionX() - 10 &&
                this.intialPositionX < pers.getIntialPositionX() + pers.getWidth() + 10) ||
                (this.intialPositionX + this.width > pers.getIntialPositionX() - 10 &&
                        this.intialPositionX + this.width < pers.getIntialPositionX() +pers.getWidth() + 10));
    }


}
