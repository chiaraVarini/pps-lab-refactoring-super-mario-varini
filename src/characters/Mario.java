package characters;

import java.awt.Image;

import javax.swing.ImageIcon;

import game.Main;
import gameObject.GameObject;
import gameObject.Piece;
import utils.Util;

public class Mario extends CharacterImpl {

	private boolean isJumping;
	private int jumpDurationAndIntenisty;
	
	public Mario(int X, int Y) {
		super(X, Y, 28 , 50);
		this.isJumping = false;
		this.jumpDurationAndIntenisty = 0;
	}

	//getters
	public boolean isJumping() {
		return isJumping;
	}

	//setters
	public void setJumping(boolean jumping) {
		this.isJumping = jumping;
	}

	// methods
	@Override
public Image walk(String nome , int frequenza ){
		
		String imageName;
		
		if(!this.isWalking() || Main.platform.getxPos() <=0 || Main.platform.getxPos() > 4600){ //se non si muove o � completamente in fondo a sinistra
			
			if(this.isTurnToRight()){ // se guarda a destra
				imageName =  nome + "AD.png";
			}else imageName = nome + "AG.png"; //se non

		}else {
			this.setStepCounter(getStepCounter()+1);
			if(this.getStepCounter() / frequenza == 0){ //
				if(this.isTurnToRight()){
					imageName =  nome + "AD.png"; // mario fermo a destra
				}else imageName =  nome + "AG.png";// mario fermo a sinistra
			}else{
				if(this.isTurnToRight()){
					imageName =  nome + "D.png"; // mario che camina verso destra
				}else imageName = nome + "G.png";// mario che camina verso sinistra
			}
			if(this.getStepCounter() == 2* frequenza )
				this.setStepCounter(0);
		}
		
		//images del personnagio
		return Util.getImageIcon(imageName).getImage();
	}


	public Image Salto(){
		
		ImageIcon ico ;
		Image img;
		String str ;
		
		this.jumpDurationAndIntenisty++;
		
		//salita
		if(this.jumpDurationAndIntenisty <= 41){
			if(this.getIntialPositionY() > Main.platform.getHauteurPlafond())
				this.setIntialPositionY(this.getIntialPositionY() - 4);
			else this.jumpDurationAndIntenisty = 42 ;
			if(this.isTurnToRight() == true)
				str = "marioSD.png";
			else str = "marioSG.png";
			
			//discesa 
		}else if (this.getIntialPositionY() + this.getHeight() < Main.platform.getySol()){
			this.setIntialPositionY(this.getIntialPositionY() + 1);
			if(this.isTurnToRight() == true)
				str = "marioSD.png";
			else str = "marioSG.png";
			
			// isJumping finito
		}else {
			if(this.isTurnToRight() == true)
				str = "marioAD.png";
			else str = "marioAG.png";
			this.isJumping = false;
			this.jumpDurationAndIntenisty = 0 ;
		}

		//reinizializzazione img mario
		return Util.getImageIcon(str).getImage() ;
				
	}
	
	public void contact (GameObject obj){
		//objectCotact orizontale
		if(super.contactAvanti(obj) && this.isTurnToRight() ||
				 (super.contactIndietro(obj))&& !this.isTurnToRight() ){
			Main.platform.setMov(0);
			this.setWalking(false);
		}
		
		//objectCotact con oggetti sotto
		if(super.contactSotto(obj) && this.isJumping) { // mario salta su un oggetto
			Main.platform.setySol(obj.getY());
		}else if (!super.contactSotto(obj)){ // mario cade sul pavimento
			Main.platform.setySol(293);  // 293 che � il valore iniziale
			if(!this.isJumping){this.setIntialPositionY(243); // altezza iniziale di mario
		}
		
		// objectCotact con un oggetto sopra
		if(contactAlto(obj)){
			Main.platform.setHauteurPlafond(obj.getY() + obj.getHeight()); // il nuovo cielo diventa il sotto del oggetto
		}else if (!super.contactAlto(obj)&& !this.isJumping){
			Main.platform.setHauteurPlafond(0); // cielo iniziale
		}	
		}
	}
	
	public boolean contactPiece(Piece piece){
		// si controla avanti indietro , a destra e a sinistra
		if(this.contactIndietro(piece) || this.contactAlto(piece) || this.contactAvanti(piece)
				||this.contactSotto(piece)){
			return true;
		}else return false;
	}
	
	public void contact(CharacterImpl pers){
		if((super.contactAvanti(pers)) || (super.contactIndietro(pers))){
			if(pers.isAlive()){
				this.setWalking(false);
		    	this.setAlive(false);
			}
			else this.setIsAlive(true);
		}else if(super.contactSotto(pers)){
			pers.setWalking(false);
			pers.setAlive(false);
		}
	}
}
