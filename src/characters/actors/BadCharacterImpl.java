package characters.actors;

import characters.CharacterImpl;
import gameObject.GameObject;

import javax.swing.*;
import java.awt.*;

/**
 * I cattivi (funghi e tartarughe sono thread a parte)
 * Created by chiaravarini on 05/03/17.
 */
public abstract class BadCharacterImpl extends CharacterImpl implements Runnable{

    private static final int THREAD_PAUSE = 15;
    private int characterTurnToRight;

    public BadCharacterImpl(int X, int Y, int width, int height, ImageIcon image) {
        super(X, Y,width , height);
        super.setTurnToRight(true);
        super.setWalking(true);
        this.characterTurnToRight = 1;

        Thread chronoFunghi = new Thread(this);
        chronoFunghi.start();
    }

    //metodi

    private void muoviti(){
        if(super.isTurnToRight() == true){this.characterTurnToRight = 1;}
        else{this.characterTurnToRight = -1; }
        super.setIntialPositionX(super.getIntialPositionX()+this.characterTurnToRight);

    }

    @Override
    public void run() {
        try{Thread.sleep(20);}
        catch(InterruptedException e){}

        while(true){
            if(this.isAlive()){
                this.muoviti();
                try{Thread.sleep(THREAD_PAUSE);}
                catch(InterruptedException e){}
            }
        }
    }

    public void objectCotact(GameObject obj){
        changeDirection(super.contactAvanti(obj), super.contactIndietro(obj));
    }

    public void characterContact(CharacterImpl pers){
        changeDirection(super.contactAvanti(pers), super.contactIndietro(pers));
    }

    private void changeDirection(final boolean contactAvanti, final boolean contactIndietro){

        if(contactAvanti && this.isTurnToRight()){
            super.setTurnToRight(false);
            this.characterTurnToRight = -1;

        }else if (contactIndietro && !this.isTurnToRight()){
            super.setTurnToRight(true);
            this.characterTurnToRight = 1;
        }
    }

    abstract public Image muore();
}
