    package characters.actors;

    import utils.Util;

    import javax.swing.*;
    import java.awt.*;

    public class Mushroom extends BadCharacterImpl {

        public Mushroom(int X, int Y){
            super(X, Y, 27, 30, Util.getImageIcon("funghiAD.png"));
        }

        @Override
        public Image muore(){
                String str;
                ImageIcon ico;
                Image img;
                if(this.isTurnToRight()){str = "funghiED.png";}
                else{str = "funghiEG.png";}
                ico =  Util.getImageIcon(str);
                img = ico.getImage();
                return img;
            }

    }
