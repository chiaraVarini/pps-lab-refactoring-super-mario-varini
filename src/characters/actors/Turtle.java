package characters.actors;

import utils.Util;

import javax.swing.*;
import java.awt.*;

public class Turtle extends BadCharacterImpl {


    public Turtle(int X, int Y) {
        super(X, Y, 43, 50, Util.getImageIcon("turtleAD.png"));
    }

    @Override
    public Image muore(){
        String str = "turtleF.png";
        ImageIcon ico;
        Image img;
        ico = Util.getImageIcon(str);
        img = ico.getImage();
        return img;
    }
}
