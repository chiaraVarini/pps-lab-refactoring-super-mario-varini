package characters;

import java.awt.Image;

import gameObject.GameObject;

public interface Character {

	public Image walk(String nome , int frequenza );

	public boolean contact_A_Destra(GameObject ogetto);
}
