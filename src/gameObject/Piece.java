package gameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Piece extends GameObject implements Runnable{

	private static final int THREAD_PAUSE = 10;
	private int contattore ;

	public Piece(int X, int Y) {
		super(X, Y, 30 , 30);
		super.objectIcon = new ImageIcon(this.getClass().getResource("/images/piece1.png"));
		super.objectImage = this.objectIcon.getImage();
	}
	
	public Image muoviti(){
		
		ImageIcon ico ;
		Image img;
		String str ;
		this.contattore++;
		if(this.contattore / 100 == 0){
			str= "/images/piece1.png";
		}else str = "/images/piec.png";
		if(this.contattore == 200 ) {this.contattore = 0;}
		ico = new ImageIcon(getClass().getResource(str));
		img =ico.getImage();
		return img;
	}

	@Override
	public void run() {
		
		try{Thread.sleep(10);}
	catch(InterruptedException e){}
			
		while(true){
			this.muoviti();
			try{Thread.sleep(THREAD_PAUSE);}
			catch(InterruptedException e){}
		}
		
	}
}
