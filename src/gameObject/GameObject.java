package gameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

import game.Main;

public class GameObject {
	
	private int width, height; //dimensio
	private int x ,y; // posizione
	
	protected Image objectImage;
	protected ImageIcon objectIcon;
	
	public GameObject(int X, int Y , int width, int height){
			
		this.x = X;
		this.y = Y;
		this.width = width;
		this.height = height;
	}


	// GETTERS
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public Image getObjectImage() {
		return objectImage;
	}


		//SETTERS
	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	// metodi
	
	public void spostamenti (){
		
		if(Main.platform.getxPos() >= 0){
			this.x = this.x - Main.platform.getMov();
		}
	}
	
}
