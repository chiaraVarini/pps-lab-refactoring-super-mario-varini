package gameObject;

import javax.swing.ImageIcon;

public class Block extends GameObject {
	
	public Block(int X, int Y) {
		
		super(X, Y, 30 ,30);
		super.objectIcon = new ImageIcon(getClass().getResource("/images/Blocco.png"));
		super.objectImage = super.objectIcon.getImage();
	}

	
}
