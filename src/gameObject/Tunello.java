 package gameObject;

import javax.swing.ImageIcon;

public class Tunello extends GameObject {

	public Tunello(int X, int Y) {
		
		super(X, Y, 43 ,65);
		super.objectIcon = new ImageIcon(getClass().getResource("/images/tunello.png"));
		super.objectImage = super.objectIcon.getImage();
	}

}
