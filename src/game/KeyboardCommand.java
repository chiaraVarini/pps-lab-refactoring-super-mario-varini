package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardCommand implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {
		
		if(Main.platform.mario.isAlive() == true){
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			
				// per non fare muovere il castello e start 
				if(Main.platform.getxPos() == -1){
					Main.platform.setxPos(0);
					Main.platform.setX1(-50);
					Main.platform.setX2(750);
				}
				Main.platform.mario.setWalking(true);
				Main.platform.mario.setTurnToRight(true);
				Main.platform.setMov(1); // si muove verso sinistra
			}else if(e.getKeyCode() == KeyEvent.VK_LEFT){
			
				if(Main.platform.getxPos() == 4601){
					Main.platform.setxPos(4600);
					Main.platform.setX1(-50);
					Main.platform.setX2(750);
				}
			
				Main.platform.mario.setWalking(true);
				Main.platform.mario.setTurnToRight(false);
				Main.platform.setMov(-1); // si muove verso destra
			}
			// salto
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Main.platform.mario.setJumping(true);
				Audio.playSound("/audio/jump.wav");
			}
			
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Main.platform.mario.setWalking(false);
		Main.platform.setMov(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

}
