package game;

import utils.Util;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

public class Audio {
	// VARIABLES
		private Clip clip;

		// CONSTRUCTEUR
		public Audio(String son){


            try {
                clip = AudioSystem.getClip();
                clip.open(Util.getAudio(son));

            } catch (LineUnavailableException e)  {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

		// GETTERS
		public Clip getClip(){return clip;}

		// METHODES
		public void play(){clip.start();}

		public void stop(){clip.stop();}


		public static void playSound(String son){
			Audio s = new Audio(son);
			s.play();
		}
}
